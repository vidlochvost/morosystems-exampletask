package com.morosystems.postgres.service;

import com.morosystems.postgres.model.User;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {

    /**
     *
     * @param id
     * @return
     */
    ResponseEntity<?> getUserById(Long id);

    /**
     *
     * @return
     */
    List<User> getUsers();

    /**
     *
     * @param user
     */
    void save(User user);

    /**
     *
     * @param id
     * @param user
     * @return
     */
    ResponseEntity<?> edit(Long id, User user);
}
