package com.morosystems.postgres.service;

import com.morosystems.postgres.model.User;
import com.morosystems.postgres.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public ResponseEntity<?> getUserById(Long id) {
        Optional<User> user = repository.findById(id);
        if(user.isPresent()){
            return new ResponseEntity<>(user.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Could not find user " + id, HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public List<User> getUsers() {
        return repository.findAll();
    }

    @Override
    public void save(User user) {
        repository.save(user);
    }

    @Override
    public ResponseEntity<?> edit(Long id, User userRequest) {
        Optional<User> user = repository.findById(id);
        if(user.isPresent()){
            User editedUser = user.get();
            editedUser.setId(userRequest.getId());
            editedUser.setName(userRequest.getName());
            return new ResponseEntity<>(repository.save(editedUser), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Could not find user " + id, HttpStatus.BAD_REQUEST);
        }
    }
}
