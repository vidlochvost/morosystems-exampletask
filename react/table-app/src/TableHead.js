import React from "react";

class TableHead extends React.Component {
    render() {
        return(
            <thead>
                <tr>
                    <th>ID</th>
                    <th>NAME</th>
                </tr>
            </thead>
        );
    }
}

export default TableHead;