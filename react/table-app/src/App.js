import React from 'react';
import Table from "./Table";

import PropTypes from 'prop-types';

import './App.css';

class App extends React.Component {
    state = [{ id: 0, name: "Example0" }, { id: 1, name: "Example1" }, { id: 2, name: "Example2" }];

    render() {
      return(
        <div className="App">
          <header className="App-header">
            <Table tableData={this.state} deleteClick={this.deleteClick}/>
          </header>
        </div>
      );
    }

    deleteClick = (id) => {
        const newArray = this.state.filter(item => item.id !== id);
        this.setState(newArray);
    }
}

App.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.number.isRequired
};

export default App;

