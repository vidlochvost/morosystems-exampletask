import React from 'react';

class DataRow extends React.Component {
    render() {
        return(
            <tr>
                <th>{this.props.rowData.id}</th>
                <th>{this.props.rowData.name}</th>
                <th>
                    <button onClick={() => this.props.deleteClick(this.props.rowData.id)}>delete</button>
                </th>
            </tr>
        );
    }
}

export default DataRow;