import React from 'react';

import DataRow from "./DataRow";
import TableHead from "./TableHead"

class Table extends React.Component {
    render() {
        return(
            <table>
                <TableHead />
                <tbody>
                    {this.props.tableData.map(prop => {
                        return <DataRow rowData={prop} deleteClick={this.props.deleteClick}/>
                    })}
                </tbody>
            </table>
        );
    }
}



export default Table;